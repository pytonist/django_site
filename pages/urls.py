from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.first_view),
    path('123', views.second_view),
]