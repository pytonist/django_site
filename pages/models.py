from datetime import datetime

from django.db import models


# Create your models here.
class Provider(models.Model):
    """
    This model
    """
    name = models.CharField(max_length=120)
    address = models.CharField(max_length=120)
    legal_type = models.CharField(max_length=120)
    add_date = models.DateTimeField(default=datetime.now())

